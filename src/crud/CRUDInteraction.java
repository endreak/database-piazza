package crud;

import java.sql.*;
import java.util.*;

import schemas.*;


/**
 * CRUDInteraction er en hjelpeklasse som håndterer all interaksjon med databasens Interaction-tabell.
 * - håndterer også Interaction-tabellens spesialisering i tabellene Thread- og Post-tabellene.
 * - har metode for å opprette tags knyttet til threads i Tag-tabellen.
 */
public class CRUDInteraction extends DBConn {
    
    public CRUDInteraction () {
        connect();
        try {
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            System.out.println("db error during setAuoCommit of CRUDFolder="+e);
            return;
        }
    }

    /**
     * Henter ut alle tråder som befinner seg i oppgitt mappe
     */
    public Collection<InteractionThread> getAllThreadsFromFolder (Folder folder) {
        Collection<InteractionThread> result = new ArrayList<InteractionThread>();
        try {
            Statement stmt = conn.createStatement();
            String query = String.format("SELECT * from thread JOIN interaction on thread.InteractionID=interaction.InteractionID where folderid='%s';", folder.folderId);      
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                InteractionThread interaction = new InteractionThread(rs.getString("InteractionId"), rs.getString("Body"), rs.getString("Email"), rs.getString("Title"), rs.getString("FolderId"));
                result.add(interaction);
            }
        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }
        return result;
    }
    
    /**
     * Setter inn et nytt interaction-objekt i databasens Interaction-tabell
     */
    public Boolean insertInteraction (Interaction interaction) {
        try {
            String SQL = String.format("INSERT INTO Interaction (Body, Email) VALUES (?,?);"); 
            
            PreparedStatement stmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, interaction.body);
            stmt.setString(2, interaction.email);
            stmt.executeUpdate();
            conn.commit();

            // Må hente ut genererte nøkler som brukes videre ved spesialisering
            ResultSet rs = stmt.getGeneratedKeys();
            while (rs.next()) {
                int interactionId = rs.getInt(1);
                interaction.setInteractionId(String.valueOf(interactionId));
            }
            return true;

        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }
        return false;
    }

    /**
     * Setter inn et nytt InteractionThread-objekt i databasens Thread-tabell
     */
    public Boolean insertThread (InteractionThread thread, String tag) {
        try {
            insertInteraction(thread);

            String SQL = String.format("INSERT INTO Thread (InteractionID, Title, FolderID) VALUES (?,?,?);"); 

            PreparedStatement stmt = conn.prepareStatement(SQL);
            stmt.setString(1, thread.interactionId);
            stmt.setString(2, thread.title);
            stmt.setString(3, thread.folderId);
            stmt.executeUpdate();
            conn.commit();

            // Vi vil opprette en tag knyttet til threaden dersom denne sendes med
            if (tag.length() > 0) {
                insertTag(thread, tag);
            }

            return true;

        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }
        return false;
    }


    /**
     * Setter inn et nytt InteractionPost-objekt i databasens Post-tabell
     */
    public boolean insertPost(InteractionPost post) {
        try {
            insertInteraction(post);

            String SQL = String.format("INSERT INTO Post (InteractionID, ThreadID) VALUES (?,?);"); 

            PreparedStatement stmt = conn.prepareStatement(SQL);
            stmt.setString(1, post.interactionId);
            stmt.setString(2, post.threadId);
            stmt.executeUpdate();
            conn.commit();

            return true;

        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }
        return false;
    }

    /**
     * Oppretter en tag knyttet til Tag-tabellen i databasen
     */
    public Boolean insertTag (InteractionThread thread, String tag) {
        try {
            String SQL = String.format("INSERT INTO Tag (ThreadID, Tag) VALUES (?,?);"); 
            
            PreparedStatement stmt = conn.prepareStatement(SQL);
            stmt.setString(1, thread.interactionId);
            stmt.setString(2, tag);
            stmt.executeUpdate();
            conn.commit();

            return true;

        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }
        return false;
    }

    /**
     * Henter ut alle Interactions fra Interaction-tabellen i database som inneholder strengen query.
     * Sjekker mot:
     *      - Interaction-tabellens Body-attributt
     *      - Thread-tabellens Title-attributt
     *      - Tag-tabellens Tag-attributt
     */
    public Collection<Interaction> searchInInteractions(String query) {
        
        Collection<Interaction> result = new ArrayList<Interaction>();
        try {
            Statement stmt = conn.createStatement();
            String SQL = 
                "SELECT DISTINCT interaction.* "+
                "FROM interaction "+
                "LEFT JOIN thread on thread.InteractionID=interaction.InteractionID "+
                "LEFT JOIN tag on tag.ThreadID=thread.InteractionID "+
                "LEFT JOIN post on post.InteractionID=interaction.InteractionID "+
                "LEFT JOIN folder on folder.FolderID=thread.FolderID "+
                "LEFT JOIN rootfolder on rootfolder.FolderID=folder.FolderID "+
                "LEFT JOIN course on course.CourseID=rootfolder.CourseID "+
                "WHERE body LIKE '%"+query+"%' "+
                    "OR title LIKE '%"+query+"%' "+
                    "OR tag.tag LIKE '%"+query+"%' "+
                "ORDER BY DatetimeCreated;";

            ResultSet rs = stmt.executeQuery(SQL);
            
            while (rs.next()) {
                Interaction interaction = new Interaction(rs.getString("InteractionId"), rs.getString("Body"), rs.getString("Email"));
                result.add(interaction);
            }

        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }
        return result;
    }
}
