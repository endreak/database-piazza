package crud;

import java.sql.*;
import java.util.*;

import schemas.*;

/**
 * CRUDCourse er en hjelpeklasse som håndterer all interaksjon med databasens Course-tabell. 
 */
public class CRUDCourse extends DBConn {
    
    public CRUDCourse () {
        connect();
        try {
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            System.out.println("db error during setAuoCommit of CRUDCourse="+e);
            return;
        }
    }

    public Collection<Course> getAllCourses (User currentUser) {
        Collection<Course> result = new ArrayList<>();
        try {
            Statement stmt = conn.createStatement();
            String query = "SELECT Course.* FROM HasCourse HC JOIN Course ON HC.courseID=course.CourseID WHERE HC.email='"+currentUser.email+"';";
            ResultSet rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                Course course = new Course(rs.getString("CourseID"), rs.getString("Name"), rs.getString("Term"));
                result.add(course);
            }
        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }
        return result;
    }
}
