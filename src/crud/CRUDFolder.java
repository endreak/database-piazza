package crud;

import java.sql.*;
import java.util.*;

import schemas.*;

/**
 * CRUDFolderer en hjelpeklasse som håndterer all interaksjon med databasens Folder-tabell.
 * - håndterer også Folder-tabellens spesialisering i SubFolder- og RootFolder-tabellene. 
 */
public class CRUDFolder extends DBConn {
    
    public CRUDFolder () {
        connect();
        try {
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            System.out.println("db error during setAuoCommit of CRUDFolder="+e);
            return;
        }
    }

    public Collection<Folder> getRootFromCourse (Course course) {
        Collection<Folder> result = new ArrayList<>();
        try {
            Statement stmt = conn.createStatement();
            String query = "SELECT folder.* from rootfolder join folder on rootfolder.FolderID=folder.FolderID where courseid='"+course.courseId+"';";
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                Folder folder = new RootFolder(rs.getString("FolderID"), rs.getString("Foldername"), rs.getBoolean(("AllowAnonymous")), course.courseId);
                result.add(folder);
            }
        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }
        return result;
    }

    public Collection<Folder> getSubfoldersFromParent (Folder root) {
        Collection<Folder> result = new ArrayList<>();

        try {
            Statement stmt = conn.createStatement();
            String query = "SELECT folder.*, rootid from subfolder join folder on subfolder.FolderID=folder.FolderID where RootID='"+root.folderId+"';";
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                Folder folder = new SubFolder(rs.getString("FolderID"), rs.getString("Foldername"), rs.getBoolean(("AllowAnonymous")), root.folderId);
                result.add(folder);
            }
        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }

        return result;
    }
}
