package crud;

import java.sql.*;
import java.util.*;

import schemas.*;

/**
 * CRUDUser er en hjelpeklasse som håndterer all interaksjon med databasens UserPiazza-tabell.
 * - Brukes til å verifisere bruker med epost og passord opp mot database
 * - Henter ut brukerinfo fra database
 */
public class CRUDUser extends DBConn {

    public CRUDUser () {
        connect();
        try {
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            System.out.println("db error during setAuoCommit of CRUDUser="+e);
            return;
        }
    }

    public Collection<User> getAllUsers () {
        Collection<User> result = new ArrayList<>();
        try {
            Statement stmt = conn.createStatement();
            String query = "SELECT * FROM piazza.userpiazza;";
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                result.add(new User(rs.getString("Email"), rs.getString("Password"), rs.getString("Name"), rs.getInt("IsInstructor")));
            }
        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }
        return result;
    }

    /**
     * Verifiserer oppgitt epost og passord opp i mot database.
     */
    public Boolean verifyUser (String email, String password) {
        try {
            Statement stmt = conn.createStatement();
            String query = String.format("SELECT * FROM piazza.userpiazza WHERE Email='%s';", email);
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {

                if (password.equals(rs.getString("UserPassword"))) {
                    return true;
                }
                else {
                    return false;
                }
            }
            System.out.println("");
        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }
        return false;
    }

    /**
     * Forsøker å finne en bruker ut i fra epost opp i mot database.
     * Konstruerer et User-objekt som returneres.
     */
    public User getUser (String email) {
        try {
            Statement stmt = conn.createStatement();
            String query = String.format("SELECT * FROM piazza.userpiazza WHERE Email='%s';", email);      
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                return new User(rs.getString("Email"), rs.getString("UserPassword"), rs.getString("Name"), rs.getInt("IsInstructor"));
            }
        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }
        return null;
    }

    /**
     * Henter ut en samling med brukere med tilhørende statistikk om 
     * antall interactions sett og opprettet
     */
    public Collection<StatUser> getStatistics () {
        Collection<StatUser> result = new ArrayList<>();

        try {
            Statement stmt = conn.createStatement();
            String query = 
                "SELECT us.email, us.name, ifnull(r.pr, 0) AS posts_read, ifnull(c.pc, 0 ) AS posts_created "+
                "FROM userpiazza us "+
                    "LEFT JOIN ( "+
                        "SELECT email, count(interactionid) AS pc "+
                        "FROM interaction "+
                        "GROUP BY email "+
                    ") c ON c.email=us.email "+
                    "LEFT JOIN ( "+
                        "SELECT email, count(interactionid) AS pr "+
                        "FROM hasviewed "+
                        "GROUP BY email "+
                    ") r ON r.email=us.email "+
                    "GROUP BY email "+
                    "ORDER BY r.pr DESC;";

            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                StatUser statUser = new StatUser(rs.getString("email"), rs.getString("name"), rs.getInt("posts_read"), rs.getInt("posts_created"));
                result.add(statUser);
            }
        } catch (Exception e) {
            System.out.println("db error during select of loper = "+e);
        }
        return result;
    }

}