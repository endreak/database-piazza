package schemas;

public class Course {
    public String courseId;
    public String name;
    public String term;

    public Course (String courseId, String name, String term) {
        this.courseId = courseId;
        this.name = name;
        this.term = term;
    }

    @Override
    public String toString () {
        String result = this.name+", "+this.term;
        return result;
    }
}
