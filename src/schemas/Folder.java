package schemas;

public class Folder {
    public String folderId;
    public String foldername;
    public Boolean AllowAnonymous;

    public Folder (String folderId, String foldername, Boolean AllowAnonymous) {
        this.folderId = folderId;
        this.foldername = foldername;
        this.AllowAnonymous = AllowAnonymous;
    }

    @Override
    public String toString () {
        String result = this.foldername;
        return result;
    }
}
