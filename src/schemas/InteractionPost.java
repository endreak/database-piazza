package schemas;

public class InteractionPost extends Interaction {
    public String threadId;

    public InteractionPost(String interactionId, String body, String email, String threadId) {
        super(interactionId, body, email);
        this.threadId = threadId;
    }
}