package schemas;

/**
 * Hjelpeklasse som benyttes i tabell under visning av brukerstatistikk.
 */
public class StatUser {

    String email;
    String name;
    Integer read;
    Integer created;

    public StatUser(String email, String name, Integer read, Integer created) {
        this.email = email;
        this.name = name;
        this.read = read;
        this.created = created;
    }

    public String getEmail() {
        return this.email;
    }

    public String getName() {
        return this.name;
    }

    public Integer getRead() {
        return this.read;
    }

    public Integer getCreated() {
        return this.created;
    }
}