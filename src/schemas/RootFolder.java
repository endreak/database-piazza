package schemas;

public class RootFolder extends Folder {
    public String courseId;

    public RootFolder (String folderId, String foldername, Boolean AllowAnonymous, String courseId) {
        super(folderId, foldername, AllowAnonymous);
        this.courseId = courseId;
    }

}