package schemas;

public class User {
    public String email;
    public String password;
    public String name;
    public Integer isSuper;

    public User (String email, String password, String name, Integer isSuper) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.isSuper = isSuper;
    }

    public void setEmail (String email) {
        this.email = email;
    }
    public void setPassword (String password) {
        this.password = password;
    }
    public void setName (String name) {
        this.name = name;
    }

    @Override
    public String toString () {
        String result = this.email+", "+this.name;
        return result;
    }
}
