package schemas;

public class SubFolder extends Folder {
    public String rootId;

    public SubFolder (String folderId, String foldername, Boolean AllowAnonymous, String rootId) {
        super(folderId, foldername, AllowAnonymous);
        this.rootId = rootId;
    }
}