package schemas;

public class Interaction {
    public String interactionId;
    public String body;
    public String email;

    public Interaction(String interactionId, String body, String email) {
        this.interactionId = interactionId;
        this.body = body;
        this.email = email;
    }

    public void setInteractionId(String interactionId) {
        this.interactionId = interactionId;
    }

    @Override
    public String toString () {
        String result = this.interactionId+", av "+this.email;
        return result;
    }

}