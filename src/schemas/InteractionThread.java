package schemas;

public class InteractionThread extends Interaction {
    public String title;
    public String folderId;

    public InteractionThread(String interactionId, String body, String email, String title, String folderId) {
        super(interactionId, body, email);
        this.title = title;
        this.folderId = folderId;
    }

    @Override
    public String toString () {
        String result = this.title+"";
        return result;
    }
}
