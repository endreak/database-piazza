import java.util.*;
import crud.CRUDUser;
import schemas.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Kontroller for visning av statistikk i GUI
 */
public class ControllerStatistics {

    CRUDUser crudUser = new CRUDUser();

    @FXML private TableView<StatUser> statList;
    @FXML private TableColumn<StatUser, String> emailCol;
    @FXML private TableColumn<StatUser, String> nameCol;
    @FXML private TableColumn<StatUser, Integer> readCol;
    @FXML private TableColumn<StatUser, Integer> createdCol;

    public ObservableList<StatUser> list = FXCollections.observableArrayList();
    
    /**
     * Initialiserer dataen i tabellvisningen.
     */
    public void initData() {
        
        Collection<StatUser> result = crudUser.getStatistics();
        Iterator<StatUser> iterator = result.iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }

        emailCol.setCellValueFactory(new PropertyValueFactory<StatUser, String>("email"));
        nameCol.setCellValueFactory(new PropertyValueFactory<StatUser, String>("name"));
        readCol.setCellValueFactory(new PropertyValueFactory<StatUser, Integer>("read"));
        createdCol.setCellValueFactory(new PropertyValueFactory<StatUser, Integer>("created"));
        statList.setItems(list);
    }

}
