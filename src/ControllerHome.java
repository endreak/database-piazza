import java.io.IOException;
import java.util.*;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import schemas.*;
import crud.*;

/**
 * Kontroller for visning av "Hjem"-siden i GUI
 */
public class ControllerHome {

    private User currentUser;
    private Collection<Course> courses;
    private Collection<Folder> rootFolders;

    // Stakken håndterer hvor en befinner seg i mappestrukturen
    private Stack<Object> prevFolders = new Stack<Object>(); 

    private Course selectedCourse;
    private Folder currentFolder;
    private InteractionThread selectedThread;

    CRUDCourse crudCourse;
    CRUDFolder crudFolder;
    CRUDInteraction crudInteraction;

    public void initData(User currentUser) {
        this.currentUser = currentUser;

        crudCourse = new CRUDCourse();
        crudFolder = new CRUDFolder();
        crudInteraction = new CRUDInteraction();

        courses = crudCourse.getAllCourses(currentUser);
        courseList.getItems().addAll(courses);
        
        // Skjuler knapp for å vise statistikk dersom bruker ikke er instruktør
        statButton.setVisible(currentUser.isSuper == 1);

    }

    /**
     * Intern hjelpemetode som oppdaterer listen over mapper i GUI.
     */
    private void updateFolderList(Course course) {
        rootFolders = crudFolder.getRootFromCourse(course);
        folderList.getItems().removeAll(folderList.getItems());
        folderList.getItems().addAll(rootFolders);
    }

    @FXML private ListView<Folder> folderList;

    @FXML private ListView<Course> courseList;
    
    @FXML private ListView<InteractionThread> threadList;
    
    /**
     * Visninger av threads
     */
    @FXML private Text threadTitle;
    
    @FXML private Text threadBody;
    
    @FXML private Label threadAuthor;
    
    /**
     * Knapp for å vise statistikk
     */
    @FXML private Button statButton;

    @FXML
    void handleOpenStatistics(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("statistics.fxml"));
            Parent viewHome = loader.load();

            Scene viewHomeScene = new Scene(viewHome);
            
            ControllerStatistics controller = loader.getController();
            controller.initData();
            
            Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
            
            window.setScene(viewHomeScene);
            window.setTitle("Statistikk over brukere");
            window.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handling som trigges idet et fag/fagkode i listen "Fag" klikkes på.
     * Oppdaterer hvilke mapper som vises.
     */
    @FXML
    void handleChooseCourse(MouseEvent event) {
        selectedCourse = courseList.getSelectionModel().getSelectedItem();
        updateFolderList(courseList.getSelectionModel().getSelectedItem());
        prevFolders.clear();
        prevFolders.add(selectedCourse);
        threadList.getItems().removeAll(threadList.getItems());
        clearCurrentThread();
    }
    
    /**
     * Knapp for å gå tilbake til rot-mappen i faget
     */
    @FXML
    void handleRootDir(ActionEvent event) {
        clearCurrentThread();
        updateFolderList(selectedCourse);
        prevFolders.clear();
        threadList.getItems().removeAll(threadList.getItems());
    }

    /**
     * Knapp for å gå inn i markert mappe
     */
    @FXML
    void handleEnterDir(ActionEvent event) {
        if (!Objects.isNull(folderList.getSelectionModel().getSelectedItem())) {
            clearCurrentThread();

            prevFolders.add(folderList.getSelectionModel().getSelectedItem());

            // Oppdaterer threads ut i fra mappen vi er i
            currentFolder = folderList.getSelectionModel().getSelectedItem();
            updateThreadList();

            Collection<Folder> newFolderList = crudFolder.getSubfoldersFromParent(folderList.getSelectionModel().getSelectedItem());
            folderList.getItems().removeAll(folderList.getItems());
            folderList.getItems().addAll(newFolderList);
        }
    }

    /**
     * Knapp for å gå ut av nåværende mappe
     */
    @FXML
    void handleExitDir(ActionEvent event) {
        if (!prevFolders.isEmpty()) {
            clearCurrentThread();
            if (prevFolders.peek() instanceof Course) {
                updateFolderList((Course) prevFolders.peek());
            }
            else if (prevFolders.peek() instanceof RootFolder) {
                currentFolder = (Folder) prevFolders.pop();
                threadList.getItems().removeAll(threadList.getItems());
                updateFolderList(selectedCourse);
            }
            else if (prevFolders.peek() instanceof SubFolder) {
                prevFolders.pop();
                currentFolder = (Folder) prevFolders.peek();
                Collection<Folder> newFolderList = crudFolder.getSubfoldersFromParent((Folder) prevFolders.peek());
                folderList.getItems().removeAll(folderList.getItems());
                folderList.getItems().addAll(newFolderList);
                updateThreadList();
            }
        }

    }

    /**
     * Handling som trigges idet en tråd i listen "Tråder i mappe" klikkes på.
     * Visningen av tråden til høyre side i GUI blir oppdaert.
     */
    @FXML
    void handleUpdateThread(MouseEvent event) {
        if (!Objects.isNull(threadList.getSelectionModel().getSelectedItem())) {
            setCurrentThread(threadList.getSelectionModel().getSelectedItem());
        }
    }

    
    /**
     * Åpner et nytt vindu hvor bruker kan legge til en ny tråd i valgt/nåværende mappe
     */
    @FXML
    void handleNewThread(ActionEvent event) throws IOException {
        if (!Objects.isNull(currentFolder)) {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("createthread.fxml"));
            Parent viewHome = loader.load();

            Scene viewHomeScene = new Scene(viewHome);
            
            ControllerCreateThread controller = loader.getController();
            controller.initData(currentUser, currentFolder);
            
            Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
            
            window.setScene(viewHomeScene);
            window.setTitle("Piazza - Opprett ny tråd");
            window.show();

        }
    }

    /**
     * Åpner et nytt vindu hvor bruker kan legge til en ny post i valgt/nåværende tråd
     */
    @FXML
    void handleNewPost(ActionEvent event) throws IOException {
        if (!Objects.isNull(selectedThread)) {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("createpost.fxml"));
            Parent viewHome = loader.load();

            Scene viewHomeScene = new Scene(viewHome);
            
            ControllerCreatePost controller = loader.getController();
            controller.initData(currentUser, selectedThread);
            
            Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
            
            window.setScene(viewHomeScene);
            window.setTitle("Piazza - Legg til post");
            window.show();
        }
    }


    /**
     * Åpner søkefelter
     */
    @FXML
    void handleSearch(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("search.fxml"));
        Parent viewSearch = loader.load();

        Scene viewSearchScene = new Scene(viewSearch);
        
        ControllerSearch controller = loader.getController();
        controller.initData(currentUser);
        
        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        
        window.setScene(viewSearchScene);
        window.setTitle("Piazza - Søk i poster");
        window.show();
    }

    /**
     * Intern metode for å oppdatere trådene i listen "Tråder i mappe"
     */
    private void updateThreadList() {
        Collection<InteractionThread> threadsInFolder = crudInteraction.getAllThreadsFromFolder(currentFolder);
        threadList.getItems().removeAll(threadList.getItems());
        threadList.getItems().addAll(threadsInFolder);
    }

    /**
     * Intern metode for å oppdatere hvilken tråd som visest i GUI.
     */
    private void setCurrentThread(InteractionThread thread) {
        this.selectedThread = thread;
        threadTitle.setText(thread.title);
        threadBody.setText(thread.body);
        threadAuthor.setText(String.format("Sendt inn av, %s", thread.email));
    }

    /**
     * Intern metode for å fjerne tråden som visest i GUI.
     */
    private void clearCurrentThread() {
        threadTitle.setText("");
        threadBody.setText("");
        threadAuthor.setText("");
    }

}
