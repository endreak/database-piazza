import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.Stage;

import schemas.*;

public class StageHandler {
    
    Stage primaryStage;
    User currentUser;

    public StageHandler (Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void setLogin() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        primaryStage.setTitle("Piazza - Logg inn");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public void loginUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
