import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import schemas.*;
import crud.*;

/**
 * Kontroller for visning av "Opprett ny tråd"-siden i GUI
 */
public class ControllerCreateThread {

    User currentUser;
    Folder currentFolder;

    CRUDInteraction crudInteraction = new CRUDInteraction();
    
    public void initData(User currentUser, Folder currentFolder) {
        this.currentUser = currentUser;
        this.currentFolder = currentFolder;
    }

    @FXML private TextField titleField;

    @FXML private Text feedbackText;

    @FXML private TextArea bodyField;

    @FXML private TextField tagField;

    /**
     * Håndterer opprettelsen av tråden
     */
    @FXML
    void handleCreateThread(ActionEvent event) throws IOException {

        String tag = tagField.getText();

        InteractionThread thread = new InteractionThread(
            "ID to be generated by database", 
            bodyField.getText(), 
            currentUser.email,
            titleField.getText(),
            currentFolder.folderId
            );

        // Dersom Thread kunne opprettes blir bruker redirected til hovedsiden
        if (crudInteraction.insertThread(thread, tag)) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("home.fxml"));
            Parent viewHome = loader.load();

            Scene viewHomeScene = new Scene(viewHome);
            
            ControllerHome controller = loader.getController();
            controller.initData(currentUser);
            
            Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
            
            window.setScene(viewHomeScene);
            window.setTitle("Piazza - Velkommen");
            window.show();
        } else {
            feedbackText.setText("Tråden kunne ikke opprettes.");
        }
    }

}
