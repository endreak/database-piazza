import java.io.IOException;

import crud.*;
import schemas.*;

import javafx.event.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.*;
import javafx.scene.text.*;
import javafx.stage.*;

/**
 * Kontroller for visning av "logg inn"-side i GUI
 */
public class ControllerLogin {

    @FXML private TextField emailField;

    @FXML private PasswordField passwordField;

    @FXML private Button loginButton;
    
    @FXML private Text feedbackText;

    /**
     * Sjekker om kombinasjonen av oppgitt epost og passord finnes i databasen vha.
     * hjelpeklassen CRUDUser.
     * Dersom kombinasjonen finnes sendes bruker til visning av "Hjem"-side.
     */
    @FXML
    void handleLogin(ActionEvent event) throws IOException {
        CRUDUser crudUser = new CRUDUser();
        if (crudUser.verifyUser(emailField.getText(), passwordField.getText())) {
            User currentUser = crudUser.getUser(emailField.getText());

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("home.fxml"));
            Parent viewHome = loader.load();

            Scene viewHomeScene = new Scene(viewHome);
            
            ControllerHome controller = loader.getController();
            controller.initData(currentUser);
            
            Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
            
            window.setScene(viewHomeScene);
            window.setTitle("Piazza - Velkommen");
            window.show();

        } else {
            feedbackText.setText("Vennligst kontroller email og passord.");
        }
    }

}
