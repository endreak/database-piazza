import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

import java.util.*;

import crud.*;
import schemas.*;

/**
 * Kontroller for visning av søk i GUI
 */
public class ControllerSearch {

    User currentUser;
    CRUDInteraction crudInteraction = new CRUDInteraction();

    
    public void initData(User currentUser) {
        this.currentUser = currentUser;
    }

    @FXML private ListView<Interaction> resultList;

    @FXML private Text interactionTitle;

    @FXML private Label interactionAuthor;

    @FXML private Text interactionBody;

    @FXML private TextField searchField;

    /**
     * Handling som tar i mot tekststreng fra søkefeltet og oppdaterer
     * visningen av listen med resultatet fra spørringen.
     */
    @FXML
    void handleSearch(ActionEvent event) {
        Collection<Interaction> searchResult = crudInteraction.searchInInteractions(searchField.getText());
        resultList.getItems().removeAll(resultList.getItems());
        resultList.getItems().addAll(searchResult);
    }

    /**
     * Handling som oppdaterer visningen et enkelt søkeresultat
     * (får se tekst i Interaction-tabellens Body-, Email- og ID-attributt)
     */
    @FXML
    void handleUpdateInteraction(MouseEvent event) {
        updateInteraction(resultList.getSelectionModel().getSelectedItem());
    }

    /**
     * Intern hjelpemetode som oppdaterer visningen av en enkelt Interaction i GUI.
     */
    private void updateInteraction(Interaction interaction) {
        interactionAuthor.setText("av "+interaction.email);
        interactionBody.setText(interaction.body);
        interactionTitle.setText("ID: "+interaction.interactionId);
    }
}
